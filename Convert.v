module Convert(

	      input clk,
	      input [15:0] data,
	      output [15:0] out
	     );


	always@(posedge clk)
		out <= data;


endmodule
