CC=gcc
DEBUG=-g
TRACE=--trace
CFLAGS=-Wall

default: verilate comp #exec 

#To convert verilog to C++ code in obj directory
verilate:
	verilator $(CFLAGS) --cc Convert.v --exe tb.cpp --stats $(TRACE)


comp:
	make -j -C  obj_dir/ -f VConvert.mk VConvert

#exec:
#	g++ $(CFLAGS) $(DEBUG) -Og -Iobj_dir -I/usr/local/share/verilator/include/vlstd \
#	-I/usr/local/share/verilator/include /usr/local/share/verilator/include/verilated.cpp \
#	 /usr/local/share/verilator/include/verilated_vcd_c.cpp obj_dir/VConvert__ALL.a tb.cpp \
#	-o convert
clean:
	rm -rf obj_dir
