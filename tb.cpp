#include "VConvert.h"
#include "verilated.h"
#include  "verilated_vcd_c.h"


int main(int argc, char **argv, char **env)
{

	int i;
	int clk;

	Verilated::commandArgs(argc,argv);
	VConvert *top = new VConvert;
	Verilated::traceEverOn(true);
	VL_PRINTF("Enabling waves...\n");
	VerilatedVcdC* tfp = new VerilatedVcdC;
        top->trace (tfp, 99);
        tfp->open ("Convert.vcd");

	top -> clk = 1;
	top -> data = 0x55;

	for(i=0; i < 20; i++)
	{
	  for(clk = 0; clk < 2; clk++)
	  {
	    tfp -> dump (2*i+clk);
	    top -> clk = ! top -> clk;
	    top -> eval();
	  }

	  top -> data = top -> data - 1;
	  if (Verilated::gotFinish())
		exit(0);	
	}

	tfp -> close();
	

	exit(0);
}

