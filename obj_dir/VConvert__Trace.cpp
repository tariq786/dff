// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_vcd_c.h"
#include "VConvert__Syms.h"


//======================

void VConvert::traceChg(VerilatedVcd* vcdp, void* userthis, uint32_t code) {
    // Callback from vcd->dump()
    VConvert* t=(VConvert*)userthis;
    VConvert__Syms* __restrict vlSymsp = t->__VlSymsp;  // Setup global symbol table
    if (vlSymsp->getClearActivity()) {
	t->traceChgThis (vlSymsp, vcdp, code);
    }
}

//======================


void VConvert::traceChgThis(VConvert__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    VConvert* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	vlTOPp->traceChgThis__2(vlSymsp, vcdp, code);
    }
    // Final
    vlTOPp->__Vm_traceActivity = 0U;
}

void VConvert::traceChgThis__2(VConvert__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    VConvert* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	vcdp->chgBit  (c+1,(vlTOPp->clk));
	vcdp->chgBus  (c+2,(vlTOPp->data),16);
	vcdp->chgBus  (c+3,(vlTOPp->out),16);
    }
}
