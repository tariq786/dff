// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VConvert.h for the primary calling header

#include "VConvert.h"          // For This
#include "VConvert__Syms.h"


//--------------------
// STATIC VARIABLES


//--------------------

VL_CTOR_IMP(VConvert) {
    VConvert__Syms* __restrict vlSymsp = __VlSymsp = new VConvert__Syms(this, name());
    VConvert* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Reset internal values
    
    // Reset structure values
    _ctor_var_reset();
}

void VConvert::__Vconfigure(VConvert__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

VConvert::~VConvert() {
    delete __VlSymsp; __VlSymsp=NULL;
}

//--------------------


void VConvert::eval() {
    VConvert__Syms* __restrict vlSymsp = this->__VlSymsp;  // Setup global symbol table
    VConvert* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    VL_DEBUG_IF(VL_PRINTF("\n----TOP Evaluate VConvert::eval\n"); );
    int __VclockLoop = 0;
    QData __Vchange = 1;
    while (VL_LIKELY(__Vchange)) {
	VL_DEBUG_IF(VL_PRINTF(" Clock loop\n"););
	vlSymsp->__Vm_activity = true;
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (VL_UNLIKELY(++__VclockLoop > 100)) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't converge");
    }
}

void VConvert::_eval_initial_loop(VConvert__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    _eval_initial(vlSymsp);
    vlSymsp->__Vm_activity = true;
    int __VclockLoop = 0;
    QData __Vchange = 1;
    while (VL_LIKELY(__Vchange)) {
	_eval_settle(vlSymsp);
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (VL_UNLIKELY(++__VclockLoop > 100)) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't DC converge");
    }
}

//--------------------
// Internal Methods

VL_INLINE_OPT void VConvert::_sequent__TOP__1(VConvert__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VConvert::_sequent__TOP__1\n"); );
    VConvert* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at Convert.v:9
    vlTOPp->out = vlTOPp->data;
}

void VConvert::_eval(VConvert__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VConvert::_eval\n"); );
    VConvert* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (((IData)(vlTOPp->clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__clk)))) {
	vlTOPp->_sequent__TOP__1(vlSymsp);
    }
    // Final
    vlTOPp->__Vclklast__TOP__clk = vlTOPp->clk;
}

void VConvert::_eval_initial(VConvert__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VConvert::_eval_initial\n"); );
    VConvert* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void VConvert::final() {
    VL_DEBUG_IF(VL_PRINTF("    VConvert::final\n"); );
    // Variables
    VConvert__Syms* __restrict vlSymsp = this->__VlSymsp;
    VConvert* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void VConvert::_eval_settle(VConvert__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VConvert::_eval_settle\n"); );
    VConvert* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

VL_INLINE_OPT QData VConvert::_change_request(VConvert__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VConvert::_change_request\n"); );
    VConvert* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // Change detection
    QData __req = false;  // Logically a bool
    return __req;
}

void VConvert::_ctor_var_reset() {
    VL_DEBUG_IF(VL_PRINTF("    VConvert::_ctor_var_reset\n"); );
    // Body
    clk = VL_RAND_RESET_I(1);
    data = VL_RAND_RESET_I(16);
    out = VL_RAND_RESET_I(16);
    __Vclklast__TOP__clk = VL_RAND_RESET_I(1);
    __Vm_traceActivity = VL_RAND_RESET_I(32);
}
