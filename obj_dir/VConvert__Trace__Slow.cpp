// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_vcd_c.h"
#include "VConvert__Syms.h"


//======================

void VConvert::trace (VerilatedVcdC* tfp, int, int) {
    tfp->spTrace()->addCallback (&VConvert::traceInit, &VConvert::traceFull, &VConvert::traceChg, this);
}
void VConvert::traceInit(VerilatedVcd* vcdp, void* userthis, uint32_t code) {
    // Callback from vcd->open()
    VConvert* t=(VConvert*)userthis;
    VConvert__Syms* __restrict vlSymsp = t->__VlSymsp;  // Setup global symbol table
    if (!Verilated::calcUnusedSigs()) vl_fatal(__FILE__,__LINE__,__FILE__,"Turning on wave traces requires Verilated::traceEverOn(true) call before time 0.");
    vcdp->scopeEscape(' ');
    t->traceInitThis (vlSymsp, vcdp, code);
    vcdp->scopeEscape('.');
}
void VConvert::traceFull(VerilatedVcd* vcdp, void* userthis, uint32_t code) {
    // Callback from vcd->dump()
    VConvert* t=(VConvert*)userthis;
    VConvert__Syms* __restrict vlSymsp = t->__VlSymsp;  // Setup global symbol table
    t->traceFullThis (vlSymsp, vcdp, code);
}

//======================


void VConvert::traceInitThis(VConvert__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    VConvert* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    vcdp->module(vlSymsp->name());  // Setup signal names
    // Body
    {
	vlTOPp->traceInitThis__1(vlSymsp, vcdp, code);
    }
}

void VConvert::traceFullThis(VConvert__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    VConvert* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	vlTOPp->traceFullThis__1(vlSymsp, vcdp, code);
    }
    // Final
    vlTOPp->__Vm_traceActivity = 0U;
}

void VConvert::traceInitThis__1(VConvert__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    VConvert* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	vcdp->declBit  (c+1,"clk",-1);
	vcdp->declBus  (c+2,"data",-1,15,0);
	vcdp->declBus  (c+3,"out",-1,15,0);
	vcdp->declBit  (c+1,"Convert clk",-1);
	vcdp->declBus  (c+2,"Convert data",-1,15,0);
	vcdp->declBus  (c+3,"Convert out",-1,15,0);
    }
}

void VConvert::traceFullThis__1(VConvert__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    VConvert* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	vcdp->fullBit  (c+1,(vlTOPp->clk));
	vcdp->fullBus  (c+2,(vlTOPp->data),16);
	vcdp->fullBus  (c+3,(vlTOPp->out),16);
    }
}
